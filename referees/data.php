<?php

require_once '../Database.php';
require_once '../OsuApi.php';

session_start();

$database = new Database();
$db = $database->getConnection();

if (!empty($_GET['query'])) {
  switch ($_GET['query']) {
    case 'matchData': matchData(); break;
    case 'lobbyData': lobbyData($db); break;
    case 'lobbies': lobbies($db); break;
    case 'mappool': mappool($db); break;
    case 'players': players($db); break;
    default: echo json_encode('ERROR: Unknown query');
  }
} else {
  echo json_encode('ERROR: Missing query');
}

function matchData() {
  if (!empty($_GET['matchid'])) {
    $osuApi = new OsuApi();
    $matchData = $osuApi->getMatch($_GET['matchid']);
    $result = new StdClass;
    $result->match = $matchData->match;
    $games = $matchData->games;
    $players = array();
    foreach ($games as &$game) {
      $beatmapObject = $osuApi->getBeatmap($game->beatmap_id);
      $game->title = $beatmapObject->title;
      $game->version = $beatmapObject->version;
      $game->beatmapset_id = $beatmapObject->beatmapset_id;
      $game->creator = $beatmapObject->creator;
      foreach ($game->scores as $score) {
        $players[$score->user_id] = '';
      }
    }
    foreach ($players as $user_id => $userObject) {
      $players[$user_id] = $osuApi->getUser($user_id);
      $players[$user_id]->score = 0;
      $players[$user_id]->accuracy = round($players[$user_id]->accuracy, 2);
      $players[$user_id]->level = round($players[$user_id]->level, 0);
    }
    foreach ($games as &$game) {
      foreach ($game->scores as &$score) {
        $score->username = $players[$score->user_id]->username;
      }
    }
    $result->games = $games;
    foreach ($result->games as &$game) {
      $scores = $game->scores;
      usort($scores, function($a, $b) {
        if ($a->pass == 0 && $b->pass == 0) {
          return $b->score - $a->score;
        }
        if ($a->pass == 0) {
          return 1;
        }
        if ($b->pass == 0) {
          return -1;
        }
        return $b->score - $a->score;
      });
      $points = 6;
      foreach ($scores as &$score) {
        $score->points = $points;
        foreach ($players as &$player) {
          if ($score->username == $player->username) {
            $player->score += $points;
            switch ($points) {
              case 6: $points = 4; break;
              case 4: $points = 3; break;
              case 3: $points = 2; break;
              case 2: $points = 0; break;
            }
          }
        }
      }
    }
    usort($players, function($a, $b) {
      return $b->score - $a->score;
    });
    $result->overall = $players;
    foreach ($result->games as &$game) {
      usort($game->scores, function($a, $b) {
        if ($a->pass == 0 && $b->pass == 0) {
          return $b->score - $a->score;
        }
        if ($a->pass == 0) {
          return 1;
        }
        if ($b->pass == 0) {
          return -1;
        }
        return $b->score - $a->score;
      });
    }
    echo json_encode($result);
  } else {
    echo json_encode('ERROR: Parameter matchid missing');
  }
}

function lobbyData($db) {
  $osuApi = new OsuApi();
  if (!empty($_GET['lobbyid'])) {
    $stmt = $db->prepare('SELECT m.id as matchid, m.name as match, t.id as tierid, t.name as tier, tz.name as timezone
      FROM lobbies l INNER JOIN matches m ON l.`match` = m.id INNER JOIN tiers t ON l.tier = t.id INNER JOIN timezones tz ON l.timezone = tz.id');
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $lobby = new StdClass;
    $lobby->id = $_GET['lobbyid'];
    $lobby->match = $rows[0]['match'];
    $lobby->tier = $rows[0]['tier'];
    $lobby->timezone = $rows[0]['timezone'];
    $matchid = $rows[0]['matchid'];
    $tierid = $rows[0]['tierid'];
    $stmt = $db->prepare('SELECT osu_user_id
      FROM players
      WHERE current_lobby = :current_lobby');
    $stmt->bindValue(':current_lobby', $lobby->id, PDO::PARAM_INT);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $lobby->players = array();
    foreach ($rows as $row) {
      $player = $osuApi->getUser($row['osu_user_id']);
      $lobby->players[] = $player;
    }
    $stmt = $db->prepare('SELECT map.beatmap_id, mod.name as modname
      FROM mappool map INNER JOIN mods mod ON map.mod = mod.id
      WHERE map.`match` = :match AND map.tier = :tier');
    $stmt->bindValue(':match', $matchid, PDO::PARAM_INT);
    $stmt->bindValue(':tier', $tierid, PDO::PARAM_INT);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $lobby->mappool = array();
    foreach ($rows as $row) {
      $map = $osuApi->getBeatmap($row['beatmap_id']);
      $map->mod = $row['modname'];
      $lobby->mappool[] = $map;
    }
    echo json_encode($lobby);
  }
}

function lobbies($db) {
  $osuApi = new OsuApi();
  $stmt = $db->prepare('SELECT l.id as lobby_id, t.id as tier_id, t.name as tier_name, tz.name as timezone
    FROM lobbies l INNER JOIN tiers t ON l.tier = t.id INNER JOIN timezones tz ON l.timezone = tz.id');
  $stmt->execute();
  $lobbies = $stmt->fetchAll(PDO::FETCH_ASSOC);
  foreach ($lobbies as &$lobby) {
    $stmt = $db->prepare('SELECT osu_user_id
      FROM players
      WHERE current_lobby = :current_lobby');
    $stmt->bindValue(':current_lobby', $lobby['lobby_id'], PDO::PARAM_INT);
    $stmt->execute();
    $players = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($players as &$player) {
      $playerObject = $osuApi->getUser($player['osu_user_id']);
      $player['pp_rank'] = $playerObject->pp_rank;
      $player['username'] = $playerObject->username;
      $player['level'] = $playerObject->level;
      $player['pp_raw'] = $playerObject->pp_raw;
      $player['accuracy'] = $playerObject->accuracy;
      $player['playcount'] = $playerObject->playcount;
    }
    usort($players, function($a, $b) {
      return $a['pp_rank'] - $b['pp_rank'];
    });
    $lobby['players'] = $players;
  }
  echo json_encode($lobbies);
}

function mappool($db) {
  $osuApi = new OsuApi();
  if (!empty($_GET['lobbyId'])) {
    $stmt = $db->prepare('SELECT map.id as pool_id, map.beatmap_id, mods.name as `mod`
      FROM mappool map INNER JOIN mods mods ON map.mod = mods.id INNER JOIN lobbies l ON map.tier = l.tier
      WHERE l.id = :id
      ORDER BY mods.sorting');
    $stmt->bindValue(':id', $_GET['lobbyId'], PDO::PARAM_INT);
    $stmt->execute();
    $mappool = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($mappool as &$map) {
      $mapObject = $osuApi->getBeatmap($map['beatmap_id']);
      $map['beatmapset_id'] = $mapObject->beatmapset_id;
      $map['title'] = $mapObject->title;
      $map['version'] = $mapObject->version;
      $map['creator'] = $mapObject->creator;
    }
    echo json_encode($mappool);
  }
}

function players($db) {
  $osuApi = new OsuApi();
  if (!empty($_GET['lobbyId'])) {
    $stmt = $db->prepare('SELECT p.osu_user_id, t.name as timezone
    FROM players p INNER JOIN timezones t ON p.timezone = t.id
    WHERE p.current_lobby = :current_lobby');
    $stmt->bindValue(':current_lobby', $_GET['lobbyId'], PDO::PARAM_INT);
    $stmt->execute();
    $players = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($players as &$player) {
      $userObject = $osuApi->getUser($player['osu_user_id']);
      $player['username'] = $userObject->username;
      $player['pp_rank'] = $userObject->pp_rank;
      $player['pp_raw'] = $userObject->pp_raw;
      $player['accuracy'] = $userObject->accuracy;
      $player['playcount'] = $userObject->playcount;
      $player['level'] = $userObject->level;
    }
    usort($players, function($a, $b) {
      return $a['pp_rank'] - $b['pp_rank'];
    });
    echo json_encode($players);
  }
}

?>