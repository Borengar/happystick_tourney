<?php

class Database {
	
	private $database_host = '';
	private $database_dbname = '';
	private $database_username = '';
	private $database_password = '';

	public function getConnection() {
		$connection = new PDO('mysql:host=' . $this->database_host . ';dbname=' . $this->database_dbname, $this->database_username, $this->database_password);
		return $connection;
	}
}

?>