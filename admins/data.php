<?php

require_once '../Database.php';
require_once '../OsuApi.php';

session_start();

$database = new Database();
$db = $database->getConnection();

$stmt = $db->prepare('SELECT a.id as userid
	FROM admins a
	WHERE a.php_session = :php_session');
$stmt->bindValue(':php_session', session_id(), PDO::PARAM_STR);
$stmt->execute();
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
if (!empty($rows[0])) {
	if (!empty($_GET['query'])) {
	  switch ($_GET['query']) {
	    case 'userUpload': userUpload($db); break;
	    case 'players': players($db); break;
	    case 'lobbies': lobbies($db); break;
	    case 'tiers': tiers($db); break;
	    case 'mappool': mappool($db); break;
	    case 'mods': mods($db); break;
	    case 'addBeatmap': addBeatmap($db); break;
	    case 'deleteBeatmap': deleteBeatmap($db); break;
	    case 'mappoolZip': mappoolZip($db); break;
	    case 'saveMappoolZip': saveMappoolZip($db); break;
	    default: echo json_encode('ERROR: Unknown query');
	  }
	} else {
	  echo json_encode('ERROR: Missing query');
	}
} else {
	echo json_encode('ERROR: You are not logged in');
}

function userUpload($db) {
	$osuApi = new OsuApi();
	$stmt = $db->prepare('SELECT id, name, lower_endpoint, upper_endpoint
		FROM tiers');
	$stmt->execute();
	$tiers = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$stmt = $db->prepare('SELECT id, name
		FROM timezones');
	$stmt->execute();
	$timezones = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$userData = json_decode(file_get_contents('php://input'));
	$stmt = $db->prepare('DELETE FROM players');
	$stmt->execute();
	$stmt = $db->prepare('DELETE FROM lobbies');
	$stmt->execute();
	foreach ($userData as $key => $value) {
		$userObject = $osuApi->getUser($value->username);
		$timezone = -1;
		foreach ($timezones as $t) {
			if ($value->timezone == $t['name']) {
				$timezone = $t['id'];
			}
		}
		$tier = -1;
		foreach ($tiers as $t) {
			if ($value->tier == $t['name']) {
				if ($value->rank < $t['upper_endpoint'] && $value->rank > $t['lower_endpoint']) {
					$tier = $t['id'];
				}
			}
		}
		if ($timezone > -1 && $tier > -1 && !empty($userObject)) {
			$stmt = $db->prepare('INSERT INTO players (osu_user_id, registered_tier, current_lobby, timezone)
				VALUES (:osu_user_id, :registered_tier, :current_lobby, :timezone)');
			$stmt->bindValue(':osu_user_id', $userObject->user_id, PDO::PARAM_INT);
			$stmt->bindValue(':registered_tier', $tier, PDO::PARAM_INT);
			$stmt->bindValue(':current_lobby', $value->lobby, PDO::PARAM_INT);
			$stmt->bindValue(':timezone', $timezone, PDO::PARAM_INT);
			$stmt->execute();
			$stmt = $db->prepare('SELECT *
				FROM lobbies
				WHERE id = :id');
			$stmt->bindValue(':id', $value->lobby, PDO::PARAM_INT);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if (empty($rows[0])) {
				$stmt = $db->prepare('INSERT INTO lobbies (id, tier, timezone)
					VALUES (:id, :tier, :timezone)');
				$stmt->bindValue(':id', $value->lobby, PDO::PARAM_INT);
				$stmt->bindValue(':tier', $tier, PDO::PARAM_INT);
				$stmt->bindValue(':timezone', $timezone, PDO::PARAM_INT);
				$stmt->execute();
			}
		} else {
			echo json_encode($value->username);
			echo '<br>';
			echo json_encode($timezone);
			echo '<br>';
			echo json_encode($tier);
			echo '<br>';
			echo json_encode($userObject);
			echo '<br><br>';
		}
	}
}

function players($db) {
	$osuApi = new OsuApi();
	$stmt = $db->prepare('SELECT p.osu_user_id, t.name as timezone
		FROM players p INNER JOIN timezones t ON p.timezone = t.id');
	$stmt->execute();
	$players = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach ($players as &$player) {
		$userObject = $osuApi->getUser($player['osu_user_id']);
		$player['username'] = $userObject->username;
		$player['pp_rank'] = $userObject->pp_rank;
		$player['pp_raw'] = $userObject->pp_raw;
		$player['accuracy'] = $userObject->accuracy;
		$player['playcount'] = $userObject->playcount;
		$player['level'] = $userObject->level;
	}
	usort($players, function($a, $b) {
		return $a['pp_rank'] - $b['pp_rank'];
	});
	echo json_encode($players);
}

function lobbies($db) {
	$osuApi = new OsuApi();
	$stmt = $db->prepare('SELECT l.id as lobby_id, t.id as tier_id, t.name as tier_name, tz.name as timezone
		FROM lobbies l INNER JOIN tiers t ON l.tier = t.id INNER JOIN timezones tz ON l.timezone = tz.id');
	$stmt->execute();
	$lobbies = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach ($lobbies as &$lobby) {
		$stmt = $db->prepare('SELECT osu_user_id
			FROM players
			WHERE current_lobby = :current_lobby');
		$stmt->bindValue(':current_lobby', $lobby['lobby_id'], PDO::PARAM_INT);
		$stmt->execute();
		$players = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach ($players as &$player) {
			$playerObject = $osuApi->getUser($player['osu_user_id']);
			$player['pp_rank'] = $playerObject->pp_rank;
			$player['username'] = $playerObject->username;
			$player['level'] = $playerObject->level;
			$player['pp_raw'] = $playerObject->pp_raw;
			$player['accuracy'] = $playerObject->accuracy;
			$player['playcount'] = $playerObject->playcount;
		}
		usort($players, function($a, $b) {
			return $a['pp_rank'] - $b['pp_rank'];
		});
		$lobby['players'] = $players;
	}
	echo json_encode($lobbies);
}

function tiers($db) {
	$stmt = $db->prepare('SELECT t.id as tierid, t.name
		FROM tiers t
		ORDER BY t.lower_endpoint ASC');
	$stmt->execute();
	echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
}

function mappool($db) {
	$osuApi = new OsuApi();
	if (!empty($_GET['tier'])) {
		$stmt = $db->prepare('SELECT map.id as pool_id, map.beatmap_id, mods.name as `mod`
			FROM mappool map INNER JOIN mods mods ON map.mod = mods.id
			WHERE map.tier = :tier
			ORDER BY mods.sorting');
		$stmt->bindValue(':tier', $_GET['tier'], PDO::PARAM_INT);
		$stmt->execute();
		$mappool = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach ($mappool as &$map) {
			$mapObject = $osuApi->getBeatmap($map['beatmap_id']);
			$map['beatmapset_id'] = $mapObject->beatmapset_id;
			$map['title'] = $mapObject->title;
			$map['version'] = $mapObject->version;
			$map['creator'] = $mapObject->creator;
		}
		echo json_encode($mappool);
	}
}

function mods($db) {
	$stmt = $db->prepare('SELECT m.id as modid, m.name
		FROM mods m
		ORDER BY m.sorting');
	$stmt->execute();
	echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
}

function addBeatmap($db) {
	if (!empty($_GET['tier']) && !empty($_GET['beatmapid']) && (!empty($_GET['mod']) || $_GET['mod'] == 0)) {
		$stmt = $db->prepare('INSERT INTO mappool (tier, beatmap_id, `mod`)
			VALUES (:tier, :beatmap_id, :mod)');
		$stmt->bindValue(':tier', $_GET['tier'], PDO::PARAM_INT);
		$stmt->bindValue(':beatmap_id', $_GET['beatmapid'], PDO::PARAM_INT);
		$stmt->bindValue(':mod', $_GET['mod'], PDO::PARAM_INT);
		$stmt->execute();
		echo json_encode('SUCCESS: Beatmap added to mappool');
	} else {
		echo json_encode('ERROR: Error adding beatmap to mappool');
	}
}

function deleteBeatmap($db) {
	if (!empty($_GET['poolid'])) {
		$stmt = $db->prepare('DELETE FROM mappool
			WHERE id = :id');
		$stmt->bindValue(':id', $_GET['poolid'], PDO::PARAM_INT);
		$stmt->execute();
		echo json_encode('SUCCESS: Beatmap deleted from mappool');
	} else {
		echo json_encode('ERROR: Error deleting beatmap from mappool');
	}
}

function mappoolZip($db) {
	if (!empty($_GET['tier'])) {
		$stmt = $db->prepare('SELECT uri
			FROM mappool_zip
			WHERE tier = :tier');
		$stmt->bindValue(':tier', $_GET['tier'], PDO::PARAM_INT);
		$stmt->execute();
		echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
	}
}

function saveMappoolZip($db) {
	if (!empty($_GET['tier']) && (!empty($_GET['uri']) || $_GET['uri'] == '')) {
		$stmt = $db->prepare('UPDATE mappool_zip
			SET uri = :uri
			WHERE tier = :tier');
		$stmt->bindValue(':uri', $_GET['uri'], PDO::PARAM_STR);
		$stmt->bindValue(':tier', $_GET['tier'], PDO::PARAM_INT);
		$stmt->execute();
		echo json_encode('SUCCESS Mappool zip link saved');
	} else {
		echo json_encode('ERROR: Error saving mappool zip link');
	}
}

?>