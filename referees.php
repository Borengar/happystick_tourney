<!DOCTYPE html>
<html>
  <head>
    <script src="bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>
    <link rel="import" href="import.html">
    <link rel="import" href="referees/referees-page.html">
    <title>HappyStick Seasonal Tour</title>
    <style is="custom-style" include="iron-flex iron-flex-alignment iron-positioning">
      html {
        font-family: Roboto;
      }
    </style>
  </head>
  <body class="fullbleed layout vertical">
    <referees-page class="flex layout vertical"></referees-page>
  </body>
</html>