<?php

require_once 'Database.php';
require_once 'OsuApi.php';

session_start();

$database = new Database();
$db = $database->getConnection();

if (!empty($_GET['query'])) {
  switch ($_GET['query']) {
    case 'tiers': tiers($db); break;
    case 'mappool': mappool($db); break;
    case 'mappoolZip': mappoolZip($db); break;
    default: echo json_encode('ERROR: Unknown query');
  }
} else {
  echo json_encode('ERROR: Missing query');
}

function tiers($db) {
	$stmt = $db->prepare('SELECT t.id as tierid, t.name
		FROM tiers t
		ORDER BY t.lower_endpoint ASC');
	$stmt->execute();
	echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
}

function mappool($db) {
	$osuApi = new OsuApi();
	if (!empty($_GET['tier'])) {
		$stmt = $db->prepare('SELECT map.id as pool_id, map.beatmap_id, mods.name as `mod`
			FROM mappool map INNER JOIN mods mods ON map.mod = mods.id
			WHERE map.tier = :tier
			ORDER BY mods.sorting');
		$stmt->bindValue(':tier', $_GET['tier'], PDO::PARAM_INT);
		$stmt->execute();
		$mappool = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach ($mappool as &$map) {
			$mapObject = $osuApi->getBeatmap($map['beatmap_id']);
			$map['beatmapset_id'] = $mapObject->beatmapset_id;
			$map['title'] = $mapObject->title;
			$map['version'] = $mapObject->version;
			$map['creator'] = $mapObject->creator;
		}
		echo json_encode($mappool);
	}
}

function mappoolZip($db) {
	if (!empty($_GET['tier'])) {
		$stmt = $db->prepare('SELECT uri
			FROM mappool_zip
			WHERE tier = :tier');
		$stmt->bindValue(':tier', $_GET['tier'], PDO::PARAM_INT);
		$stmt->execute();
		echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
	}
}