<?php

require_once 'Database.php';

session_start();
$database = new Database();
$db = $database->getConnection();
$stmt = $db->prepare('SELECT p.id as userid
	FROM players p
	WHERE p.php_session = :php_session');
$stmt->bindValue(':php_session', session_id(), PDO::PARAM_STR);
$stmt->execute();
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
if (isset($rows[0]['userid'])) {
	header('Location: players.php');
}
$stmt = $db->prepare('SELECT a.id as userid
	FROM admins a
	WHERE a.php_session = :php_session');
$stmt->bindValue(':php_session', session_id(), PDO::PARAM_STR);
$stmt->execute();
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
if (isset($rows[0]['userid'])) {
	header('Location: admins.php');
}

?>

<!DOCTYPE html>
<html>
	<head>
		<script src="bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>
		<link rel="import" href="import.html">
		<link rel="import" href="index-page.html">
		<title>HappyStick Seasonal Tour</title>
		<style is="custom-style" include="iron-flex iron-flex-alignment iron-positioning">
			html {
				font-family: Roboto;
			}
		</style>
	</head>
	<body class="fullbleed layout vertical">
		<index-page class="flex layout vertical"></index-page>
	</body>
</html>