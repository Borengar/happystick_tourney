<?php

require_once 'TwitchApi.php';
require_once 'Database.php';

session_start();

if (!empty($_GET['code']) && !empty($_GET['state'])) {
	$database = new Database();
	$db = $database->getConnection();
	$twitchApi = new TwitchApi();
	$accessToken = $twitchApi->getAccessToken($_GET['code'], $_GET['state']);
	$userData = $twitchApi->getUser($accessToken);
	$stmt = $db->prepare('SELECT a.id as userid
		FROM admins a
		WHERE a.twitch_user_id = :twitch_user_id');
	$stmt->bindValue(':twitch_user_id', $userData->_id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	if (!empty($rows[0]['userid'])) {
		// twitch user is already known as admin
		$stmt = $db->prepare('UPDATE admins
			SET php_session = :session, twitch_access_token = :twitch_access_token, twitch_username = :twitch_username
			WHERE id = :id');
		$stmt->bindValue(':session', session_id(), PDO::PARAM_STR);
		$stmt->bindValue(':twitch_access_token', $accessToken, PDO::PARAM_STR);
		$stmt->bindValue(':twitch_username', $userData->display_name, PDO::PARAM_STR);
		$stmt->bindValue(':id', $rows[0]['userid'], PDO::PARAM_INT);
		$stmt->execute();
		header('Location: admins.php');
	} else {
		// twitch user is new user
		$stmt = $db->prepare('INSERT INTO twitch_logins (twitch_user_id, twitch_username, twitch_access_token)
			VALUES (:twitch_user_id, :twitch_username, :twitch_access_token)');
		$stmt->bindValue(':twitch_user_id', $userData->_id, PDO::PARAM_INT);
		$stmt->bindValue(':twitch_username', $userData->display_name, PDO::PARAM_STR);
		$stmt->bindValue(':twitch_access_token', $accessToken, PDO::PARAM_STR);
		$stmt->execute();
		header('Location: index.php');
	}
}

?>