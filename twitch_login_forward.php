<?php

require_once 'TwitchApi.php';

$twitchApi = new TwitchApi();
$uri = $twitchApi->getLoginUri();
header('Location: ' . $uri);
die();

?>