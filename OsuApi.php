<?php

require_once 'Database.php';

class OsuApi {

  // get api key from https://osu.ppy.sh/p/api
  private $osu_api_key = '';

  public function getBeatmap($beatmap_id) {
    $database = new Database();
    $db = $database->getConnection();
    $stmt = $db->prepare('SELECT id, approved, approved_date, last_update, artist, beatmap_id, beatmapset_id, bpm, creator, difficultyrating, diff_size, diff_overall, diff_approach, diff_drain, hit_length, source, genre_id, language_id, title, total_length, version, file_md5, mode, tags, favourite_count, playcount, passcount, max_combo
      FROM beatmaps 
      WHERE beatmap_id = :beatmap_id');
    $stmt->bindValue(':beatmap_id', $beatmap_id, PDO::PARAM_INT);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if (!empty($rows[0])) {
      $response = new StdClass;
      $response->approved = $rows[0]['approved'];
      $response->approved_date = $rows[0]['approved_date'];
      $response->last_update = $rows[0]['last_update'];
      $response->artist = $rows[0]['artist'];
      $response->beatmap_id = $rows[0]['beatmap_id'];
      $response->beatmapset_id = $rows[0]['beatmapset_id'];
      $response->bpm = $rows[0]['bpm'];
      $response->creator = $rows[0]['creator'];
      $response->difficultyrating = $rows[0]['difficultyrating'];
      $response->diff_size = $rows[0]['diff_size'];
      $response->diff_overall = $rows[0]['diff_overall'];
      $response->diff_approach = $rows[0]['diff_approach'];
      $response->diff_drain = $rows[0]['diff_drain'];
      $response->hit_length = $rows[0]['hit_length'];
      $response->source = $rows[0]['source'];
      $response->genre_id = $rows[0]['genre_id'];
      $response->language_id = $rows[0]['language_id'];
      $response->title = $rows[0]['title'];
      $response->total_length = $rows[0]['total_length'];
      $response->version = $rows[0]['version'];
      $response->file_md5 = $rows[0]['file_md5'];
      $response->mode = $rows[0]['mode'];
      $response->tags = $rows[0]['tags'];
      $response->favourite_count = $rows[0]['favourite_count'];
      $response->playcount = $rows[0]['playcount'];
      $response->passcount = $rows[0]['passcount'];
      $response->max_combo = $rows[0]['max_combo'];
    } else {
      $request_url = 'https://osu.ppy.sh/api/get_beatmaps?k=' . $this->osu_api_key . '&b=' . urlencode($beatmap_id);
      $response = json_decode(file_get_contents($request_url))[0];
      $stmt = $db->prepare('INSERT INTO beatmaps (approved, approved_date, last_update, artist, beatmap_id, beatmapset_id, bpm, creator, difficultyrating, diff_size, diff_overall, diff_approach, diff_drain, hit_length, source, genre_id, language_id, title, total_length, version, file_md5, mode, tags, favourite_count, playcount, passcount, max_combo)
        VALUES (:approved, :approved_date, :last_update, :artist, :beatmap_id, :beatmapset_id, :bpm, :creator, :difficultyrating, :diff_size, :diff_overall, :diff_approach, :diff_drain, :hit_length, :source, :genre_id, :language_id, :title, :total_length, :version, :file_md5, :mode, :tags, :favourite_count, :playcount, :passcount, :max_combo)');
      $stmt->bindValue(':approved', $response->approved, PDO::PARAM_STR);
      $stmt->bindValue(':approved_date', $response->approved_date, PDO::PARAM_STR);
      $stmt->bindValue(':last_update', $response->last_update, PDO::PARAM_STR);
      $stmt->bindValue(':artist', $response->artist, PDO::PARAM_STR);
      $stmt->bindValue(':beatmap_id', $response->beatmap_id, PDO::PARAM_INT);
      $stmt->bindValue(':beatmapset_id', $response->beatmapset_id, PDO::PARAM_INT);
      $stmt->bindValue(':bpm', $response->bpm, PDO::PARAM_INT);
      $stmt->bindValue(':creator', $response->creator, PDO::PARAM_STR);
      $stmt->bindValue(':difficultyrating', $response->difficultyrating, PDO::PARAM_STR);
      $stmt->bindValue(':diff_size', $response->diff_size, PDO::PARAM_STR);
      $stmt->bindValue(':diff_overall', $response->diff_overall, PDO::PARAM_STR);
      $stmt->bindValue(':diff_approach', $response->diff_approach, PDO::PARAM_STR);
      $stmt->bindValue(':diff_drain', $response->diff_drain, PDO::PARAM_STR);
      $stmt->bindValue(':hit_length', $response->hit_length, PDO::PARAM_INT);
      $stmt->bindValue(':source', $response->source, PDO::PARAM_STR);
      $stmt->bindValue(':genre_id', $response->genre_id, PDO::PARAM_INT);
      $stmt->bindValue(':language_id', $response->language_id, PDO::PARAM_INT);
      $stmt->bindValue(':title', $response->title, PDO::PARAM_STR);
      $stmt->bindValue(':total_length', $response->total_length, PDO::PARAM_INT);
      $stmt->bindValue(':version', $response->version, PDO::PARAM_STR);
      $stmt->bindValue(':file_md5', $response->file_md5, PDO::PARAM_STR);
      $stmt->bindValue(':mode', $response->mode, PDO::PARAM_INT);
      $stmt->bindValue(':tags', $response->tags, PDO::PARAM_STR);
      $stmt->bindValue(':favourite_count', $response->favourite_count, PDO::PARAM_INT);
      $stmt->bindValue(':playcount', $response->playcount, PDO::PARAM_INT);
      $stmt->bindValue(':passcount', $response->passcount, PDO::PARAM_INT);
      $stmt->bindValue(':max_combo', $response->max_combo, PDO::PARAM_INT);
      $stmt->execute();
    }
    return $response;
  }

  public function getUser($username) {
    $database = new Database();
    $db = $database->getConnection();
    $stmt = $db->prepare('SELECT id, user_id, username, count300, count100, count50, playcount, ranked_score, total_score, pp_rank, level, pp_raw, accuracy, count_rank_ss, count_rank_s, count_rank_a, country, pp_country_rank, cache_update
      FROM osu_users
      WHERE user_id = :user_id OR username = :username');
    $stmt->bindValue(':user_id', $username, PDO::PARAM_STR);
    $stmt->bindValue(':username', $username, PDO::PARAM_STR);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if (!empty($rows[0])) {
      $cacheTime = new DateTime($rows[0]['cache_update']);
      $timeDifference = $cacheTime->diff(new DateTime());
      if ($timeDifference->d >= 1) {
        $request_url = 'https://osu.ppy.sh/api/get_user?k=' . $this->osu_api_key . '&u=' . urlencode($username);
      $response = json_decode(file_get_contents($request_url))[0];
        $stmt = $db->prepare('UPDATE osu_users
          SET count300 = :count300, count100 = :count100, count50 = :count50, playcount = :playcount, ranked_score = :ranked_score, total_score = :total_score, pp_rank = :pp_rank, level = :level, pp_raw = :pp_raw, accuracy = :accuracy, count_rank_ss = :count_rank_ss, count_rank_s = :count_rank_s, count_rank_a = :count_rank_a, country = :country, pp_country_rank = :pp_country_rank, cache_update = NOW()
          WHERE user_id = :user_id OR username = :username');
        $stmt->bindValue(':user_id', $response->user_id, PDO::PARAM_INT);
        $stmt->bindValue(':username', $response->username, PDO::PARAM_STR);
        $stmt->bindValue(':count300', $response->count300, PDO::PARAM_INT);
        $stmt->bindValue(':count100', $response->count100, PDO::PARAM_INT);
        $stmt->bindValue(':count50', $response->count50, PDO::PARAM_INT);
        $stmt->bindValue(':playcount', $response->playcount, PDO::PARAM_INT);
        $stmt->bindValue(':ranked_score', $response->ranked_score, PDO::PARAM_INT);
        $stmt->bindValue(':total_score', $response->total_score, PDO::PARAM_INT);
        $stmt->bindValue(':pp_rank', $response->pp_rank, PDO::PARAM_INT);
        $stmt->bindValue(':level', round($response->level, 0), PDO::PARAM_INT);
        $stmt->bindValue(':pp_raw', $response->pp_raw, PDO::PARAM_STR);
        $stmt->bindValue(':accuracy', round($response->accuracy, 2), PDO::PARAM_STR);
        $stmt->bindValue(':count_rank_ss', $response->count_rank_ss, PDO::PARAM_INT);
        $stmt->bindValue(':count_rank_s', $response->count_rank_s, PDO::PARAM_INT);
        $stmt->bindValue(':count_rank_a', $response->count_rank_a, PDO::PARAM_INT);
        $stmt->bindValue(':country', $response->country, PDO::PARAM_STR);
        $stmt->bindValue(':pp_country_rank', $response->pp_country_rank, PDO::PARAM_INT);
        $stmt->execute();
      } else {
        $response = new StdClass;
        $response->user_id = $rows[0]['user_id'];
        $response->username = $rows[0]['username'];
        $response->count300 = $rows[0]['count300'];
        $response->count100 = $rows[0]['count100'];
        $response->count50 = $rows[0]['count50'];
        $response->playcount = $rows[0]['playcount'];
        $response->ranked_score = $rows[0]['ranked_score'];
        $response->total_score = $rows[0]['total_score'];
        $response->pp_rank = $rows[0]['pp_rank'];
        $response->level = $rows[0]['level'];
        $response->pp_raw = $rows[0]['pp_raw'];
        $response->accuracy = $rows[0]['accuracy'];
        $response->count_rank_ss = $rows[0]['count_rank_ss'];
        $response->count_rank_s = $rows[0]['count_rank_s'];
        $response->count_rank_a = $rows[0]['count_rank_a'];
        $response->country = $rows[0]['country'];
        $response->pp_country_rank = $rows[0]['pp_country_rank'];
      }
    } else {
      $request_url = 'https://osu.ppy.sh/api/get_user?k=' . $this->osu_api_key . '&u=' . urlencode($username);
      $response = json_decode(file_get_contents($request_url))[0];
      $stmt = $db->prepare('INSERT INTO osu_users (user_id, username, count300, count100, count50, playcount, ranked_score, total_score, pp_rank, level, pp_raw, accuracy, count_rank_ss, count_rank_s, count_rank_a, country, pp_country_rank, cache_update)
        VALUES (:user_id, :username, :count300, :count100, :count50, :playcount, :ranked_score, :total_score, :pp_rank, :level, :pp_raw, :accuracy, :count_rank_ss, :count_rank_s, :count_rank_a, :country, :pp_country_rank, NOW())');
      $stmt->bindValue(':user_id', $response->user_id, PDO::PARAM_INT);
      $stmt->bindValue(':username', $response->username, PDO::PARAM_STR);
      $stmt->bindValue(':count300', $response->count300, PDO::PARAM_INT);
      $stmt->bindValue(':count100', $response->count100, PDO::PARAM_INT);
      $stmt->bindValue(':count50', $response->count50, PDO::PARAM_INT);
      $stmt->bindValue(':playcount', $response->playcount, PDO::PARAM_INT);
      $stmt->bindValue(':ranked_score', $response->ranked_score, PDO::PARAM_INT);
      $stmt->bindValue(':total_score', $response->total_score, PDO::PARAM_INT);
      $stmt->bindValue(':pp_rank', $response->pp_rank, PDO::PARAM_INT);
      $stmt->bindValue(':level', round($response->level, 0), PDO::PARAM_INT);
      $stmt->bindValue(':pp_raw', $response->pp_raw, PDO::PARAM_STR);
      $stmt->bindValue(':accuracy', round($response->accuracy, 2), PDO::PARAM_STR);
      $stmt->bindValue(':count_rank_ss', $response->count_rank_ss, PDO::PARAM_INT);
      $stmt->bindValue(':count_rank_s', $response->count_rank_s, PDO::PARAM_INT);
      $stmt->bindValue(':count_rank_a', $response->count_rank_a, PDO::PARAM_INT);
      $stmt->bindValue(':country', $response->country, PDO::PARAM_STR);
      $stmt->bindValue(':pp_country_rank', $response->pp_country_rank, PDO::PARAM_INT);
      $stmt->execute();
    }
    return $response;
  }

  public function getMatch($match_id) {
    $database = new Database();
    $db = $database->getConnection();
    $request_url = 'https://osu.ppy.sh/api/get_match?k=' . $this->osu_api_key . '&mp=' . urlencode($match_id);
    $response = json_decode(file_get_contents($request_url));
    foreach ($response->games as &$game) {
      $stmt = $db->prepare('SELECT name
        FROM mods
        WHERE id = :id');
      $stmt->bindValue(':id', $game->mods, PDO::PARAM_INT);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (!empty($rows[0]['name'])) {
        $game->mod = $rows[0]['name'];
      }
    }
    return $response;
  }

}

?>